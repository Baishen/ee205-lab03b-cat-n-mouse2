echo I am
whoami
echo The current working directory is
pwd
echo The system I am on is
uname -n
echo The Linux version is
uname -r
echo The Linux distribution is
cat /etc/os-release | grep 'PRETTY_NAME' | cut -c 14- | cut -c -29
echo The system has been up for
uptime
echo "The amount of disk space I'm using in KB is"
du -k ~ | tail -n 1

